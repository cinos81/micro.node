const http = require('http');
const url = require('url');
const querystring = require('querystring');

const onRequest = (res, method, pathname, params) => res.end(JSON.stringify(params));

http.createServer((req, res) => {
	const method = req.method;
	const uri = url.parse(req.url, true);
	const pathname = uri.pathname;

	if (method === 'POST' || method === 'PUT') {
		let body = '';

		req.on('data', data => body += data);
		req.on('end', () => {
			let params = '';
			if (req.headers['content-type'] === 'application/json') {
				params = JSON.parse(body);
			} else {
				params = querystring.parse(body);
			}

			onRequest(res, method, pathname, params);
		})
	} else {
		onRequest(res, method, pathname, uri.query);
	}
}).listen(8000);