const http = require('http');
const url = require('url');
const querystring = require('querystring');

class MyServer {
	constructor(req, res) {
		this.req = req;
		this.res = res;
		this.body = '';
		this.params = {};
		this.run();
	}

	get method() {
		return this.req.method;
	}

	get uri() {
		return url.parse(this.req.url, true);
	}

	get pathname() {
		return this.uri.pathname;
	}

	get isPOST() {
		return this.method === 'POST';
	}

	get isPUT() {
		return this.method === 'PUT';
	}

	get contentType() {
		return this.req.headers['content-type'];
	}

	get isJSONType() {
		return this.contentType === 'application/json';
	}

	get response() {
		return JSON.stringify(this.params);
	}

	onRequest() {
		this.res.end(this.response);
	}

	onData(chunk) {
		this.body = this.body + chunk;
	}

	onEnd() {
		if (this.isJSONType) {
			this.params = JSON.parse(this.body);
		} else {
			this.params = querystring.parse(this.body);
		}
		this.onRequest();
	}

	run() {
		if (this.isPOST || this.isPUT) {
			this.req.on('data', chunk => this.onData(chunk));
			this.req.on('end', () => this.onEnd());
		} else {
			this.onRequest();
		}
	}
}

http.createServer((req, res) => new MyServer(req, res)).listen(8000);
