const http = require('http');

function request(cb, params) {
	const options = {
		host: '127.0.0.1',
		port: 8000,
		headers: {
			'Content-Type': 'application/json'
		}
	};

	const req = http.request(options, res => {
		let data = '';
		res.on('data', chunk => data += chunk);
		res.on('end', () => {
			console.log(options, data);
			cb();
		});
	});

	if (params) {
		req.write(JSON.stringify(params));
	}

	req.end();
}