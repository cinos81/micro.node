const net = require('net');

const server = net.createServer(socket => socket.end('hello world'));
server.on('error', console.log);
server.listen(9000, () => console.log('listen', server.address()));