const mysql = require('mysql');

class Communicator {
	static ConnInfo = {
		host: 'localhost',
		user: 'root',
		password: 'example',
		database: 'monolithic'
	};

	transaction(sql, onComplete, cb) {
		this.connection = mysql.createConnection(Communicator.ConnInfo);
		this.connection.connect();
		this.connection.query(sql, (error, results, fields) => onComplete(error, results, fields, cb));
		this.connection.end();
	}
}

module.exports = Communicator;
