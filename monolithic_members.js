const Communicator = require('./Communicator');

module.exports = {
	communicator: new Communicator(),
	registerSql(params) {
		const { username, password } = params;
		const sql =
			`insert into members(username, password)
			 values('${username}', password('${password}'));
			`;
		return sql;
	},
	onRegister(error, results, fields, cb) {
		let response = error ? {
			errorcode: 1,
			errormessage: 'error'
		} : {
			errorcode: 0,
			errormessage: 'success'
		};
		cb(response);
	},
	register(method, pathname, params, cb) {
		if (params.username === null || params.password === null) {
			cb({
				errorcode: 1,
				errormessage: 'Invalid Parameters'
			});
		} else {
			const registerSql = this.registerSql(params);
			this.communicator.transaction(registerSql, this.onRegister, cb);
		}
	},
	unregisterSql(param) {
		return `delete from members where username = ${param.username};`;
	},
	onUnregister(error, results, fields, cb) {
		let response = error ? {
			errorcode: 1,
			errormessage: 'error'
		} : {
			errorcode: 0,
			errormessage: 'success'
		};
		cb(response);
	},
	unregister(method, pathname, params, cb) {
		this.communicator.unregisterSql(this.unregisterSql(params), this.onRegister, cb);
	},
	getInquirySql(param) {
		const { username, password } = param;
		return
		`select id from members
		 where 
		 username = ${username} 
		 and 
		 password=password('${password}');
		`;
	},
	onInquiry(error, results = [], fields, cb) {
		let response = {};
		if (error || response.results.length === 0) {
			response = {
				errorcode: 1,
				errormessage: error ? error : 'invalid password'
			};
		} else {
			response = {
				key: 'temp',
				userid: results[0]['id'],
				errorcode: 0,
				errormessage: 'success'
			}
		}
		cb(response);
	},
	inquiry(method, pathname, params, cb) {
		this.communicator.transaction(this.getInquirySql(params), this.onInquiry, cb);
	},
	onRequest(res, method, pathname, params, cb) {
		let rst = null;
		switch (method) {
			case 'POST': {
				rst = this.register(method, pathname, params, response => process.nextTick(cb, res, response));
			} break;

			case 'GET': {
				rst = this.inquiry(method, pathname, params, response => process.nextTick(cb, res, response));
			} break;

			case 'DELETE': {
				rst = this.unregister(method, pathname, params, response => process.nextTick(cb, res, response));
			} break;

			default: {
				rst = process.nextTick(cb, res, null);
			}
		}
		return rst
	}
};