const mysql = require('mysql');
const conn = {
	host: 'localhost',
	user: 'root',
	port: 8080,
	password: 'example',
	database: 'monolithic'
};

const connection = mysql.createConnection(conn);
connection.connect();
connection.query('query', (err, rst, fields) => {
	console.log(err, rst, fields);
});
connection.end();