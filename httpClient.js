const http = require('http');

const options = {
	host: '127.0.0.1',
	port: 8000,
	path: '/'
};


const req = http.request(options, res => {
	let data = '';
	res.on('data', chunk => {
		console.log('chunk', chunk);
		data += chunk;
	}).on('end', () => console.log(data));
});

req.end();