const Communicator = require('./Communicator');

module.exports = {
	communicator: new Communicator(),
	registerSql(params) {
		const { name, category, price, description } = params;
		const sql =
			`insert into goods(name, category, price, description)
			 values(${name}, ${category}, ${price}, ${description});
			`;
		return sql;
	},
	onRegister(error, results, fields, cb) {
		let response = error ? {
				errorcode: 1,
				errormessage: 'error'
			} : {
				errorcode: 0,
				errormessage: 'success'
			};
		cb(response);
	},
	register(method, pathname, params, cb) {
		if (params.name === null || params.category === null || params.price === null || params.description === null) {
			cb({
				errorcode: 1,
				errormessage: 'Invalid Parameters'
			});
		} else {
			const registerSql = this.registerSql(params);
			this.communicator.transaction(registerSql, this.onRegister, cb);
		}
	},
	unregisterSql(param) {
		return `delete from goods where id = ${param.id};`;
	},
	onUnregister(error, results, fields, cb) {
		let response = error ? {
			errorcode: 1,
			errormessage: 'error'
		} : {
			errorcode: 0,
			errormessage: 'success'
		};
		cb(response);
	},
	unregister(method, pathname, params, cb) {
		if (params.id === null) {
			cb({
				errorcode: 1,
				errormessage: 'Invalid Parameters'
			});
		} else {
			this.communicator.unregisterSql(this.unregisterSql(params), this.onRegister, cb);
		}
	},
	get inquirySql() {
		return 'select * from goods';
	},
	onInquiry(error, results = [], fields, cb) {
		let response = { results };
		if (error || response.results.length === 0) {
			response = {
				errorcode: 1,
				errormessage: 'no data'
			};
		}
		cb(response);
	},
	inquiry(method, pathname, params, cb) {
		this.communicator.transaction(this.inquirySql, this.onInquiry, cb);
	},
	onRequest(res, method, pathname, params, cb) {
		let rst = null;
		switch (method) {
			case 'POST': {
				rst = this.register(method, pathname, params, response => process.nextTick(cb, res, response));
			} break;

			case 'GET': {
				rst = this.inquiry(method, pathname, params, response => process.nextTick(cb, res, response));
			} break;

			case 'DELETE': {
				rst = this.unregister(method, pathname, params, response => process.nextTick(cb, res, response));
			} break;

			default: {
				rst = process.nextTick(cb, res, null);
			}
		}
		console.log('리턴');
		return rst
	}
};