const http = require('http');
const url = require('url');
const querystring = require('querystring');
const members = require('./monolithic_members');
const goods = require('./monolithic_goods');
const purchases = require('./monolithic_purchases');

class ServerInitializer {
	constructor(req, res) {
		this.req = req;
		this.res = res;
		this.body = '';
		this.data = null;

		if (this.isPost || this.isPut) {
			this.req.on('data', data => this.onData(data));
			this.req.on('end', () => this.onEnd());
		} else {
			this.onRequest(this.uri.query);
		}
	}

	get method() {
		return this.req.method;
	}

	get pathname() {
		return this.uri.pathname;
	}

	get uri() {
		return url.parse(this.req.url, true);
	}

	get isPost() {
		return this.method === 'POST';
	}

	get isPut() {
		return this.method === 'PUT';
	}

	onData(data) {
		this.data = this.data + data;
	}

	get contentType() {
		return this.req.headers['content-type'];
	}

	get isJSON() {
		return this.contentType === 'application/json';
	}

	onEnd() {
		let params = '';
		if (this.isJSON) {
			params = JSON.parse(this.body);
		} else {
			params = querystring.parse(this.body);
		}

		this.onRequest(params);
	}

	response(res, packet) {
		res.writeHead(200, { 'Content-Type': 'application/json' });
		res.end(JSON.stringify(packet));
	}

	onRequest(params) {
		const { res, method, pathname } = this;
		switch (this.pathname) {
			case '/members': {
				members.onRequest(res, method, pathname, params, this.response);
			} break;

			case '/goods': {
				goods.onRequest(res, method, pathname, params, this.response);
			} break;

			case '/purchases': {
				purchases.onRequest(res, method, pathname, params, this.response);
			} break;

			default: {
				this.res.writeHead(404);
				this.res.end()
			} break;
		}
	}
}

http.createServer((req, res) => new ServerInitializer(req, res)).listen(8000);
