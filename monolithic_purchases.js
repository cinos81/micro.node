const Communicator = require('./Communicator');

module.exports = {
	communicator: new Communicator(),
	registerSql(params) {
		const { userid, goodsid } = params;
		const sql =
			`insert into purchases(userid, goodsid)
			 values(${userid}, ${goodsid});
			`;
		return sql;
	},
	onRegister(error, results, fields, cb) {
		let response = error ? {
			errorcode: 1,
			errormessage: 'error'
		} : {
			key: 'inserttemp',
			errorcode: 0,
			errormessage: 'success'
		};
		cb(response);
	},
	register(method, pathname, params, cb) {
		if (params.name === null || params.category === null || params.price === null || params.description === null) {
			cb({
				errorcode: 1,
				errormessage: 'Invalid Parameters'
			});
		} else {
			const registerSql = this.registerSql(params);
			this.communicator.transaction(registerSql, this.onRegister, cb);
		}
	},
	getInquirySql(params) {
		return `select id, goodsid, date from purchases where userid =${params.userid}`;
	},
	onInquiry(error, results = [], fields, cb) {
		let response = { results };
		if (error || response.results.length === 0) {
			response = {
				errorcode: 1,
				errormessage: 'no data'
			};
		}
		cb(response);
	},
	inquiry(method, pathname, params, cb) {
		this.communicator.transaction(this.inquirySql, this.onInquiry, cb);
	},
	onRequest(res, method, pathname, params, cb) {
		let rst = null;
		switch (method) {
			case 'POST': {
				rst = this.register(method, pathname, params, response => process.nextTick(cb, res, response));
			} break;

			case 'GET': {
				rst = this.inquiry(method, pathname, params, response => process.nextTick(cb, res, response));
			} break;

			default: {
				rst = process.nextTick(cb, res, null);
			}
		}
		return rst
	}
};